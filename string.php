<?php
echo 'this is single quoted string<br>';
?>
<?php
echo "this is double quoted string<br>";
?>
<?php
$str = <<<EOD
example of string spanning multipple lines using heredoc syntax
EOD;
echo $str;
?>
<br>
<?php
$str = <<<'EOD'
example of string spanning multipple lines using nowdoc syntax
EOD;
echo $str;
?>
