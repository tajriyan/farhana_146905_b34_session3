//testing floatval function
<?php
$flot_str="1234.369html";
$flot=floatval($flot_str);//only returns float value
echo $flot;
?>


// Testing empty function
<?php
$var1 = '';
$var2="fine";
var_dump(empty($var1));//its for empty function
echo "<br>";
var_dump(empty($var2));//its for empty function
echo "<br>";
?>

//Testing is_array function
<?php
$x = array('this', 'is', 'an array');//its for is_array function
$y ='this is a string';
var_dump(is_array($x));
echo "<br>";
var_dump(is_array($y));
echo "<br>";
?>


//Testing is_null function
<?php
$x = null;//its for is_null function
$y =0;
var_dump(is_null($x));
echo "<br>";
var_dump(is_null($y));
echo "<br>";
var_dump(is_null($z));//variable not declared
echo "<br>";
?>


//testing is_object function
<?php
$obj = (object) array('name' => 'farhana', 'class' => '5');
$obj1 = array('Kalle', 'Ross', 'Felipe');
var_dump(is_object($obj));
echo "<br>";
var_dump(is_object($obj1));
echo "<br>";
?>

//testing isset function
<?php
$a = "test";
$b = "anothertest";
$c =null;
var_dump(isset($a));      // TRUE
var_dump(isset($a, $b));
echo "<br>";
unset($a);
var_dump(isset($a));
echo "<br>";
var_dump(isset($c));//test null variable
echo "<br>";
?>
//testing serialize and unserialze function
<?php
$array =array("val1","val2","val3");
$xtr=serialize($array);
echo $xtr;
echo "<br>";
$unsrl=unserialize($xtr);
print_r($unsrl);
//var_dump($unsrl);//it shows data type and value
echo "<br>";
?>
//testing print_r function
<pre>
<?php
$a = array ('a'=> 'apple', 'b'=>'banana', 'c'=>array ('x', 'y', 'z'));
print_r ($a);
?>
</pre>


//testing unset function
<?php
echo "<br>";
$a ="happy world";
echo $a;
echo "<br>";
var_dump((unset)$a);
echo "<br>";
?>


//testing var_dump function
<?php
$var="1234.369";
$Var=1234;
var_dump($var);//only returns float value
echo "<br>";
var_dump($Var);
?>
//testing var_export function
<?php
$a = array ('a'=> 'apple', 'b'=>'banana', 'c'=>array ('x', 'y', 'z'));
var_export($a);
?>
